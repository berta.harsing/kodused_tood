package homeworktests;


import homework.Sorting;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class SortingTests {


    @Test
    public void should_return_not_sort_and_return_empty_array_when_array_is_empty(){
        int[] arr = {};
        assertEquals("should_return_null_when_array_is_empty", arr, Sorting.sorting(arr));
    }

    @Test
    public void should_sort_and_return_sorted_array_when_array_has_two_elements(){
        int[] arr = {4, 3};
        assertEquals("should return array with sorted elements", Arrays.toString(new int[]{3, 4}), Arrays.toString(Sorting.sorting(arr)));
    }

    @Test
    public void should_sort_and_return_already_sorted_array_when_array_has_two_elements(){
        int[] arr = {3, 4};
        assertEquals("should return array with sorted elements", Arrays.toString(new int[]{3, 4}), Arrays.toString(Sorting.sorting(arr)));
    }

    @Test
    public void should_sort_and_return_already_sorted_array_when_array_has_three_elements(){
        int[] arr = {3, 4, 5};
        assertEquals("should return array with sorted elements", Arrays.toString(new int[]{3, 4, 5}), Arrays.toString(Sorting.sorting(arr)));
    }

    @Test
    public void should_sort_and_return_sorted_array_when_array_has_three_elements(){
        int[] arr = {4, 3, 1};
        assertEquals("should return array with sorted elements", Arrays.toString(new int[]{1, 3, 4}), Arrays.toString(Sorting.sorting(arr)));
    }

    @Test
    public void should_sort_and_return_sorted_array_which_has_a_lot_elements() {
        int[] arr = { 1, 2, 5, 4, 3, 66, 45, 88, 112, 78, 99, 55, 90, 76, 42};
        assertEquals("should return array with sorted elements:", Arrays.toString(new int[]
                {1, 2, 3, 4, 5, 42, 45, 55, 66, 76, 78, 88, 90, 99, 112}), Arrays.toString(Sorting.sorting(arr)));
    }


}
