package homeworktests;


import homework.PositiveEvenSum;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PositiveEvenSumTests {

    @Test
    public void should_return_the_only_element_of_array() {
        int[] arr = {2};
        assertEquals("Array has only one element and should return : ", 2, PositiveEvenSum.sumEven(arr, (arr.length-1)));
    }

    @Test
    public void should_return_sum_of_some_positive_even_numbers() {
        int[] arr = {2, 4, 6};
        assertEquals("Array should return  even numbers sum: ", 12, PositiveEvenSum.sumEven(arr, (arr.length-1)));
    }

    @Test
    public void should_return_only_sum_of_positive_even_numbers() {
        int[] arr =  {-2, -4, 2, 4, 6};
        assertEquals("Array should return even numbers sum", 12, PositiveEvenSum.sumEven(arr, (arr.length-1)));
    }

    @Test
    public void should_not_read_odd_numbers_while_doing_sum() {
        int[] arr =  {1, 2, 4, 6};
        assertEquals("Array should return only sum of even numbers" , 12, PositiveEvenSum.sumEven(arr, (arr.length-1)));
    }

    @Test
    public void should_not_read_odd_numbers_while_doing_sum_only_even() {
        int[] arr = {-2, -4, 1, 2, 4, 6};
        assertEquals("Array should return only sum of even numbers", 12, PositiveEvenSum.sumEven(arr, (arr.length-1)));
    }
}
