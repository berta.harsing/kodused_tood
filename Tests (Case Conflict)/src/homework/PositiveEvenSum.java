package homework;


public class PositiveEvenSum {

    public static int sumEven(int[] arr, int index) {
        int sum;
        if (index == -1) {
            return 0;
        } else if(arr[index] < 0) {
            sum = sumEven(arr, --index);
        }
        else if (arr[index] % 2 == 0 ) {
            sum = arr[index] + sumEven(arr, --index);
        } else {
            sum =  sumEven(arr, --index);
        }
        return sum;
    }

    public static void main(String[] args) {
        int[] array = {1, -2, 3, 4, 5, 6, 7, 8};
        System.out.println(sumEven(array, (array.length-1)));
    }
    
}
