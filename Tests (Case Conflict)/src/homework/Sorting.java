package homework;


import java.util.Arrays;

public class Sorting {

    public static int[] sorting(int[] array) {
        if (array.length == 0) {
            return array;
        }
        int temp;
        for (int i = 0; i < array.length-1; i++) {
            for (int j = 1; j < array.length-i; j++) {
                if (array[j-1] > array[j]) {
                    temp = array[j-1];
                    array[j-1] = array[j];
                    array[j] = temp;
                }
            }
        }
        Arrays.toString(array);
        return array;
    }
}